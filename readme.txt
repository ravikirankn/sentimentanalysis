This is a project on sentiment analysis of movie reviews.

The problem statement is here:
https://www.kaggle.com/c/sentiment-analysis-on-movie-reviews

with this current code: we scored a score of 0.63472 and are currently (6th December 2014) ranked at 128
bitbucket url for the code: https://bitbucket.org/ravikirankn/sentimentanalysis

the code uses several classifiers and out of the those, randomforrest classifier generates the maximum score.

These are the dependencies:
sklearn, nltk, numpy, python3.*
sudo pip install <package_name> (Eg:sudo pip install sklearn)

Running the code:
python RunProgram.py

Output:

Program starting..
Running RandomForestClassifier on the provided data set
accuracy of the trained model: 98.808104886769968
accuracy 5 fold corss_validtion 62.77442702050664%
accuracy 20 fold predictor.accuracy 63.25693606755129%
Time taken to classify(in secs): 23.08
----------------------------
Running BaggingClassifier on the provided data set
accuracy of the trained model: 97.377830750893921
accuracy 5 fold corss_validtion 61.59227985524729%
accuracy 20 fold predictor.accuracy 62.27985524728587%
Time taken to classify(in secs): 29.51
----------------------------
Running AdaBoostClassifier on the provided data set
accuracy of the trained model: 76.281287246722286
accuracy 5 fold corss_validtion 64.89746682750301%
accuracy 20 fold predictor.accuracy 62.90711700844391%
Time taken to classify(in secs): 28.76
----------------------------
Running GradientBoostingClassifier on the provided data set
accuracy of the trained model: 96.781883194278905
accuracy 5 fold corss_validtion 61.399276236429436%
accuracy 20 fold predictor.accuracy 63.564535585042215%
Time taken to classify(in secs): 102.86
----------------------------


output_RandomForestClassifier.csv is the submission file: 
it has the sentiment assigned towards the test data, after classification is done.

Scores when other classifiers are used:
BaggingClassifier -- 0.63071
AdaBoostClassifier -- 0.61047
RandomForestClassifier -- 0.63472
GradientBoostingClassifier -- 0.61940 
