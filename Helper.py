from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.ensemble import BaggingClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.pipeline import make_pipeline, make_union
from Features import LowerCaseAndTokenisedTransformation, ClassifierForFeatures
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics import accuracy_score
from collections import defaultdict

_valid_classifiers = {
    "BaggingClassifier": BaggingClassifier,
    "AdaBoostClassifier": AdaBoostClassifier,
    "RandomForestClassifier": RandomForestClassifier,
    "GradientBoostingClassifier": GradientBoostingClassifier
}

def target(phrases):
    return [datapoint.sentiment for datapoint in phrases]

class SentimentAnalysis:
    def __init__(self, classifier="sgd"):
        classifier = _valid_classifiers[classifier](**{})
        # lower case transformation
        pipeline = [LowerCaseAndTokenisedTransformation()]
        
        # features extrction
        ext = [build_text_extraction()]
        ext = make_union(*ext)
        pipeline.append(ext)
        
        self.pipeline = make_pipeline(*pipeline)
        self.classifier = classifier
        
    def fit(self, phrases, y=None):
        y = target(phrases)
        Z = self.pipeline.fit_transform(phrases, y)
        # print(self.pipeline)
        # print(y)
        # print(Z)
        # print(self.classifier)
        self.classifier.fit(Z, y)
        self.accuracy = self.classifier.score(Z, y)
        return self
    
    def predict(self, phrases):
        Z = self.pipeline.transform(phrases)
        labels = self.classifier.predict(Z)
        return labels
    
    def accuracy(self):
        return self.accuracy

    def score(self, phrases):
        pred = self.predict(phrases)
        return accuracy_score(target(phrases), pred)

    def error_matrix(self, phrases):
        predictions = self.predict(phrases)
        matrix = defaultdict(list)
        for phrase, predicted in zip(phrases, predictions):
            if phrase.sentiment != predicted:
                matrix[(phrase.sentiment, predicted)].append(phrase)
        return matrix
        
        
def build_text_extraction():
    return make_pipeline(CountVectorizer(binary=True,
                                tokenizer=lambda x: x.split(),
                                min_df=0,
                                ngram_range=(1, 1),
                                stop_words=None),
                         ClassifierForFeatures())
