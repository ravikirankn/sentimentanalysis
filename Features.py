import nltk
import numpy
from sklearn.linear_model import SGDClassifier
from sklearn.multiclass import fit_ovo

class Transformation:
    def fit(self, X, y=None):
        return self

"""
X is a lis of data points
returns tokenized words seperated by , and lowercase words
"""  
class LowerCaseAndTokenisedTransformation(Transformation):
    def transform(self, X):
        it = (" ".join(nltk.word_tokenize(datapoint.phrase)) for datapoint in X)
        return [x.lower() for x in it]

"""
SGDClassifier is used,
this reduces the list of words into features.
"""
class ClassifierForFeatures:
    def fit(self, X, y):
        self.classifiers = fit_ovo(SGDClassifier(), X, numpy.array(y), n_jobs=-1)[0]
        return self

    def transform(self, X, y=None):
        xs = [clf.decision_function(X).reshape(-1, 1) for clf in self.classifiers]
        return numpy.hstack(xs)