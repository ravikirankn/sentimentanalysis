from collections import namedtuple
import os
import csv
from Helper import SentimentAnalysis
import random
import time

Datapoint = namedtuple("Datapoint", "phraseid sentenceid phrase sentiment")

"""
add these in the classifiers: you can add 1 or all, don't modify these.

_classifiers = ["RandomForestClassifier", "BaggingClassifier", "AdaBoostClassifier", "GradientBoostingClassifier"]

"""
_classifiers = ["RandomForestClassifier"]


def _iter_data_file(filename):
    path = os.path.join(filename)
    it = csv.reader(open(path, "r"), delimiter="\t")
    row = next(it)  # Drop column names
    if " ".join(row[:3]) != "PhraseId SentenceId Phrase":
        raise ValueError("Input file has wrong column names: {}".format(path))
    for row in it:
        if len(row) == 3:
            row += (None,)
        yield Datapoint(*row)
        
def get_train_data(__cached=[]):
    if not __cached:
        __cached.extend(_iter_data_file("train.tsv"))
    return __cached

def get_test_data():
    return list(_iter_data_file("test.tsv"))

def write_output_csv(test, prediction, fileAppend):
    with open('output_' + fileAppend + '.csv', 'w') as csvfile:
        writer1 = csv.DictWriter(csvfile, fieldnames=["PhraseId", "Sentiment"])
        writer1.writerow({'PhraseId':"PhraseId", 'Sentiment':"Sentiment"})
        for datapoint, sentiment in zip(test, prediction):
            writer1.writerow({'PhraseId':datapoint.phraseid, 'Sentiment':sentiment})
    print("saved the classification on test data set into output.csv")


def modelTestData(classfier, predictor):
    test = list(get_test_data())
    prediction = predictor.predict(test)
    write_output_csv(test, prediction, classfier)

"""
commented cross validations because they consume lot of time.
uncomment them if you wanna used them
"""
def classifyData(classfier):
    start = time.time()
    print("Running " + classfier + " on the provided data set")
    predictor = SentimentAnalysis(classfier)
    predictor.fit(list(get_train_data()))
    end = time.time()
    modelTestData(classfier, predictor)
    print("accuracy of the trained model: " + repr(predictor.accuracy * 100))
    # print("accuracy 5 fold corss_validtion {}%".format(cross_validation(list(get_train_data()), classfier, 5) * 100))
    # print("accuracy 20 fold predictor.accuracy {}%".format(cross_validation(list(get_train_data()), classfier, 20) * 100))
    return end - start

def cross_validation(data, classfier, folds):
    factory = lambda: SentimentAnalysis(classfier)
    scores = []
    for k in range(folds):
        random.shuffle(data)
        train = data[:90]
        test = data[10:]
        predictor = factory()
        predictor.fit(train)
        score = predictor.score(test)
        scores.append(score)
    return sum(scores) / len(scores)

"""
Program starts here
"""

print("Program starting..")
for key in _classifiers:
    time1 = classifyData(key)
    print("Time taken to classify(in secs): " + str(time1))
    print("----------------------------")

